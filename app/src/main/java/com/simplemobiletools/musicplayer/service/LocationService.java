package com.simplemobiletools.musicplayer.service;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.simplemobiletools.musicplayer.activities.MainActivity;
import com.simplemobiletools.musicplayer.entity.PlaceInfo;

import java.util.concurrent.TimeUnit;

/**
 * Created by Moluram on 3/30/2017.
 */

public class LocationService {
    private static LocationService instance;
    private PlaceLikelihood placeLikelihood;
    private LocationService() {}
    public static synchronized LocationService getInstance() {
        if (instance == null) {
            instance = new LocationService();
        }
        return instance;
    }

    public void guessCurrentPlace() {
        if (ActivityCompat.checkSelfPermission(MainActivity.me ,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(MainActivity.getmApiClient(), null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(@NonNull PlaceLikelihoodBuffer likelyPlaces ) {
                if (likelyPlaces.getStatus().isSuccess()) {
                    placeLikelihood = likelyPlaces.get(0);
                    TargetEntityService.getInstance().setPlaceInfo(
                            new PlaceInfo()
                                    .setLatLng(placeLikelihood.getPlace().getLatLng())
                                    .setTypesOfPlace(placeLikelihood.getPlace().getPlaceTypes())
                    );
                    likelyPlaces.release();
                } else {
                    return;
                }
            }
        }, 10L, TimeUnit.SECONDS);
    }
}
