package com.simplemobiletools.musicplayer.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.simplemobiletools.musicplayer.MusicService;

/**
 * Service for recognition user activity
 * @author Aliaksei Chorny
 */
public class ActivityRecognizedService extends IntentService {

    private DetectedActivity trueActivity;

    IBinder mBinder = new ActivityRecognizedService.LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public ActivityRecognizedService getInstance() {
            return ActivityRecognizedService.this;
        }
    }

    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public DetectedActivity getUserActivity() {
        return trueActivity;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            trueActivity = result.getMostProbableActivity();
        }
    }
}
