package com.simplemobiletools.musicplayer.service;

import com.simplemobiletools.musicplayer.entity.Interval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Moluram on 3/30/2017.
 */

public class IntervalService {
    private static IntervalService instance;

    private IntervalService(List<Interval> list) {
        this.pairs = list;
    }

    public static synchronized IntervalService getInstance() {
        if (instance == null) {
            instance = new IntervalService(new ArrayList<Interval>());
        }
        return instance;
    }

    private List<Interval> pairs;

    public int getTotalLength(){
        Collections.sort(pairs);
        List<Interval> merged = new ArrayList<>();
        if (pairs != null && !pairs.isEmpty()) {
            merged.add(pairs.get(0));
        }

        for (int i = 1; i < pairs.size(); i++){
            int size = merged.size();
            Interval lastInter = merged.get(size - 1);
            if (lastInter.overlap(pairs.get(i))) {
                lastInter.setRight(Math.max(lastInter.getRight(), pairs.get(i).getRight()));
            } else {
                merged.add(pairs.get(i));
            }
        }

        int sum = 0;
        for (Interval inter : merged) {
            sum += inter.getRight() - inter.getLeft();
        }
        pairs.clear();
        return sum;
    }

    public void init() {
        pairs.clear();
        pairs.add(new Interval(0, null));
    }


    public void addRight(Integer right) {
        if (!pairs.isEmpty()) {
            pairs.get(pairs.size() - 1).setRight(right);
        }
    }

    public void addLeft(Integer left) {
        if (pairs.isEmpty() || pairs.get(pairs.size() - 1).getRight() != null) {
            pairs.add(new Interval(left, null));
        }
    }

}
