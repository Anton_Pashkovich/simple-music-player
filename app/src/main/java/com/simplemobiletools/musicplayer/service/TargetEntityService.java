package com.simplemobiletools.musicplayer.service;

import android.os.Environment;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.location.DetectedActivity;
import com.simplemobiletools.musicplayer.entity.Song;
import com.simplemobiletools.musicplayer.entity.User;
import com.simplemobiletools.musicplayer.entity.ArousalValenceData;
import com.simplemobiletools.musicplayer.entity.PlaceInfo;
import com.simplemobiletools.musicplayer.entity.TargetEntity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

public class TargetEntityService {

    private static TargetEntityService instance;

    private TargetEntity targetEntity;
    // here we can inject other serbices for property extracting

    private TargetEntityService() {
        this.targetEntity = new TargetEntity();
    }

    public static synchronized TargetEntityService getInstance() {
        if (instance == null) {
            instance = new TargetEntityService();
        }
        return instance;
    }

    // implement methods for property extracting
    // it's preferred to use injected services

    public void saveArousalValence(List<ArousalValenceData> values) {
        List<ArousalValenceData> avList = this.targetEntity.getPlaybackInfo().getAvList();
        avList.addAll(values);
    }

    public void serialize() {
        final File path = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC + "/MUSIC_LOG/");

        if(!path.exists()) {
            path.mkdirs();
        }

        final File file = new File(path, "music_log.txt");
        ObjectMapper mapper = new ObjectMapper();

        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            if (file.length() == 0) {
                myOutWriter.append("[");
            } else  {
                myOutWriter.append(",");
            }
            String jsonValue = mapper.writeValueAsString(targetEntity);
            myOutWriter.append(jsonValue);

            myOutWriter.close();
            fOut.flush();
            fOut.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public void clear() {
        User user = targetEntity.getUser();
        this.targetEntity = new TargetEntity();
        this.targetEntity.setUser(user);
    }

    public void setDetectedActivity(DetectedActivity detectedActivity) {
        this.targetEntity.setUserActivity(detectedActivity);
    }

    public void setUserInfo(User user) {
        targetEntity.setUser(user);
    }

    public void fillMetaData(Song currentSong) {
        this.targetEntity.setSong(currentSong);
    }

    public void songIsRewind() {
        targetEntity.getPlaybackInfo().setRewind(true);
    }

    public void setSkipped() {
        targetEntity.getPlaybackInfo().setSkipped(true);
    }

    public void setPlaceInfo(PlaceInfo placeInfo) {
        this.targetEntity.setPlaceInfo(placeInfo);
    }

    public void setProgress(int progress, int duration) {
        this.targetEntity.getPlaybackInfo().setProgress((double) progress / duration);
    }
}