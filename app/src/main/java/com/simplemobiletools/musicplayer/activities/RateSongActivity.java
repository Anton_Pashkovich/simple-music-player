package com.simplemobiletools.musicplayer.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.simplemobiletools.musicplayer.MusicService;
import com.simplemobiletools.musicplayer.R;
import com.simplemobiletools.musicplayer.entity.ArousalValenceData;
import com.simplemobiletools.musicplayer.service.TargetEntityService;

import java.util.ArrayList;

public class RateSongActivity extends AppCompatActivity {

    PointsGraphSeries<DataPoint> xySeries;

    private Button btnSave;

    private SeekBar arousalSeekBar, valenceSeekBar;

    private TextView arousalText, valenceText;

    GraphView mScatterPlot;

    private TargetEntityService service = TargetEntityService.getInstance();

    private ArrayList<ArousalValenceData> xyValueArray;

    LocalBroadcastManager mLocalBroadcastManager;

    boolean mBounded;

    MusicService mMusicService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_song);
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("closeRateActivity");
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mIntentFilter);
        //declare variables in oncreate
        btnSave = (Button) findViewById(R.id.btnAddPt);
        mScatterPlot = (GraphView) findViewById(R.id.scatterPlot);
        arousalSeekBar = (SeekBar) findViewById(R.id.arousal);
        valenceSeekBar = (SeekBar) findViewById(R.id.valence);
        arousalText = (TextView) findViewById(R.id.arousalValue);
        valenceText = (TextView) findViewById(R.id.valenceValue);
        xyValueArray = new ArrayList<>();

        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);
    }

    private void init() {
        xySeries = new PointsGraphSeries<>();
        arousalText.setText(String.valueOf(normalizeSeekBarValue(arousalSeekBar.getProgress())));
        valenceText.setText(String.valueOf(normalizeSeekBarValue(valenceSeekBar.getProgress())));

        arousalSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                arousalText.setText(String.valueOf(normalizeSeekBarValue(seekBar.getProgress())));
                double x = Double.parseDouble(valenceText.getText().toString());
                double y = Double.parseDouble(arousalText.getText().toString());
                xyValueArray.clear();
                xyValueArray.add(new ArousalValenceData(x, y, mMusicService.getCurrentPosition()));
                updateScatterPlot();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        valenceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                valenceText.setText(String.valueOf(normalizeSeekBarValue(seekBar.getProgress())));
                double x = Double.parseDouble(valenceText.getText().toString());
                double y = Double.parseDouble(arousalText.getText().toString());
                xyValueArray.clear();
                xyValueArray.add(new ArousalValenceData(x, y, mMusicService.getCurrentPosition()));
                updateScatterPlot();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (ArousalValenceData av : xyValueArray) {
                    if (av.getCurrentPosition() == null && mMusicService != null) {
                        av.setCurrentPosition(mMusicService.getCurrentPosition());
                    }
                }
                service.saveArousalValence(xyValueArray);
                finish();
            }
        });

        createScatterPlot();
    }

    private void createScatterPlot() {
        //add the data to the series
        xyValueArray.add(new ArousalValenceData(0.0, 0.0));
        for(int i = 0; i < xyValueArray.size(); i++){
            double x = xyValueArray.get(i).getX();
            double y = xyValueArray.get(i).getY();
            xySeries = new PointsGraphSeries<>();
            xySeries.appendData(new DataPoint(x,y),true, 1);
        }
        //set some properties
        xySeries.setShape(PointsGraphSeries.Shape.POINT);
        xySeries.setColor(Color.BLUE);
        xySeries.setSize(20f);
        //set manual x bounds
        mScatterPlot.getViewport().setYAxisBoundsManual(true);
        mScatterPlot.getViewport().setMaxY(1);
        mScatterPlot.getViewport().setMinY(-1);
        //set manual y bounds
        mScatterPlot.getViewport().setXAxisBoundsManual(true);
        mScatterPlot.getViewport().setMaxX(1);
        mScatterPlot.getViewport().setMinX(-1);

        mScatterPlot.removeAllSeries();
        mScatterPlot.addSeries(xySeries);
    }

    private void updateScatterPlot() {
        for(int i = 0; i < xyValueArray.size(); i++){
            double x = xyValueArray.get(i).getX();
            double y = xyValueArray.get(i).getY();
            xySeries = new PointsGraphSeries<>();
            xySeries.appendData(new DataPoint(x,y),true, 1);
        }
        mScatterPlot.removeAllSeries();
        mScatterPlot.addSeries(xySeries);
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals("closeRateActivity")){
                finish();
            }
        }
    };

    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    private double normalizeSeekBarValue(int progress) {
        double value = (double) progress / 100 - 1;
        return Math.round(value * 100.0) / 100.0;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent mIntent = new Intent(this, MusicService.class);
        bindService(mIntent, mConnection, BIND_AUTO_CREATE);
    }

    ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(RateSongActivity.this, "Service is disconnected", Toast.LENGTH_SHORT).show();
            mBounded = false;
            mMusicService = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            mBounded = true;
            MusicService.LocalBinder mLocalBinder = (MusicService.LocalBinder)service;
            mMusicService = mLocalBinder.getMusicServiceInstance();
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        if(mBounded) {
            unbindService(mConnection);
            mBounded = false;
        }
    }

}
