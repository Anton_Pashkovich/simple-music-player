package com.simplemobiletools.musicplayer.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.simplemobiletools.musicplayer.R;
import com.simplemobiletools.musicplayer.entity.User;
import com.simplemobiletools.musicplayer.service.TargetEntityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserInfoActivity extends SimpleActivity {

    Button buttonSave;
    String[] age = {"", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75"};
    ArrayList<CheckBox> musicEmotions;

    TargetEntityService service = TargetEntityService.getInstance();

    private static Map<Integer, Integer> preferencesMap;

    static {
        preferencesMap = new HashMap<>();
        preferencesMap.put(R.id.check_box_sad, 0);
        preferencesMap.put(R.id.check_box_exciting, 1);
        preferencesMap.put(R.id.check_box_angry, 2);
        preferencesMap.put(R.id.check_box_happy, 3);
        preferencesMap.put(R.id.check_box_sleepy, 4);
        preferencesMap.put(R.id.check_box_relaxing, 5);
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        final TextView firstName = (TextView) findViewById(R.id.edit_name);
        final TextView secondName = (TextView) findViewById(R.id.edit_surname);
        final TextView patronymic = (TextView) findViewById(R.id.edit_patronymic);
        final TextView homeTown = (TextView) findViewById(R.id.editHometown);
        final RadioGroup genderRadioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        musicEmotions = new ArrayList<>();
        musicEmotions.add((CheckBox) findViewById(R.id.check_box_sad));
        musicEmotions.add((CheckBox) findViewById(R.id.check_box_exciting));
        musicEmotions.add((CheckBox) findViewById(R.id.check_box_angry));
        musicEmotions.add((CheckBox) findViewById(R.id.check_box_happy));
        musicEmotions.add((CheckBox) findViewById(R.id.check_box_sleepy));
        musicEmotions.add((CheckBox) findViewById(R.id.check_box_relaxing));

        // адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, age);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final Spinner ageSpinner = (Spinner) findViewById(R.id.ageSpinner);
        ageSpinner.setAdapter(adapter);
        // заголовок
        ageSpinner.setPrompt("Your age");

        buttonSave = (Button) findViewById(R.id.save_button);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Integer> preferences = new ArrayList<>();
                for (int i = 0; i < musicEmotions.size(); i++){
                    if (musicEmotions.get(i).isChecked()){
                        preferences.add(preferencesMap.get(musicEmotions.get(i).getId()));
                    }
                }
                String firstUserName = String.valueOf(firstName.getText());
                String secondUserName = String.valueOf(secondName.getText());
                String thirdUserName = String.valueOf(patronymic.getText());
                String userHomeTown = String.valueOf(homeTown.getText());
                int selectedId = genderRadioGroup.getCheckedRadioButtonId();

                String userGender = null;
                RadioButton genderRadioButton = (RadioButton) findViewById(selectedId);
                if (genderRadioButton != null) {
                    userGender = String.valueOf(genderRadioButton.getText());
                }
                Integer userAge = null;
                String userAgeString = ageSpinner.getSelectedItem().toString();
                if (userAgeString != null && userAgeString != "") {
                    userAge = Integer.valueOf(userAgeString);
                }
                service.setUserInfo(new User(firstUserName, secondUserName, thirdUserName, userAge, userHomeTown, userGender, preferences));
                finish();
            }
        });
    }
}