package com.simplemobiletools.musicplayer.observer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.simplemobiletools.musicplayer.activities.MainActivity;
import com.simplemobiletools.musicplayer.service.ActivityRecognizedService;
import com.simplemobiletools.musicplayer.service.LocationService;
import com.simplemobiletools.musicplayer.service.TargetEntityService;
/**
 * Class serve for set user activity at each call
 * @author Aliaksei Chorny
 */
public class MusicServiceUserActivityObserver implements Observer {

    ActivityRecognizedService activityService;

    boolean mBounded;

    public MusicServiceUserActivityObserver(Context mContext) {
        Intent mIntent = new Intent(mContext, ActivityRecognizedService.class);
        mContext.bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void handleEvent() {
        MainActivity.GoogleApiConnect();
        activityHandler();
        placeHandler();
    }

    private void placeHandler() {
        LocationService.getInstance().guessCurrentPlace();
    }

    private void activityHandler() {
        TargetEntityService.getInstance().setDetectedActivity(
                activityService.getUserActivity());
    }

    ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            mBounded = false;
            activityService = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            mBounded = true;
            ActivityRecognizedService.LocalBinder mLocalBinder = (ActivityRecognizedService.LocalBinder)service;
            activityService = mLocalBinder.getInstance();
        }
    };
}
