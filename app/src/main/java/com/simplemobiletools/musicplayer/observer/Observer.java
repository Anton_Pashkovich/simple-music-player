package com.simplemobiletools.musicplayer.observer;

/**
 * Class represent an realisation of pattern Observer
 * @author Aliaksei Chorny
 */
public interface Observer {
    void handleEvent();
}
