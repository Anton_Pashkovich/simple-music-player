package com.simplemobiletools.musicplayer.observer;

/**
 * Class represent an realisation of part of pattern Observer
 * @author Aliaksei Chorny
 */
public interface Observable {
    void addObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObserver();
}
