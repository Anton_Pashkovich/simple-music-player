package com.simplemobiletools.musicplayer.entity;

import com.google.android.gms.location.DetectedActivity;

public class TargetEntity {

    private User                user;
    private Song                song;
    private PlaybackInfo        playbackInfo;
    private DetectedActivity    userActivity;
    private PlaceInfo           placeInfo;



    public TargetEntity() {
        user = new User();
        playbackInfo = new PlaybackInfo();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public PlaybackInfo getPlaybackInfo() {
        return playbackInfo;
    }

    public void setPlaybackInfo(PlaybackInfo playbackInfo) {
        this.playbackInfo = playbackInfo;
    }

    public DetectedActivity getUserActivity() {
        return userActivity;
    }

    public void setUserActivity(DetectedActivity userActivity) {
        this.userActivity = userActivity;
    }

    public PlaceInfo getPlaceInfo() {
        return placeInfo;
    }

    public void setPlaceInfo(PlaceInfo placeInfo) {
        this.placeInfo = placeInfo;
    }
}