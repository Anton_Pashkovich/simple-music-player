package com.simplemobiletools.musicplayer.entity;

import java.util.ArrayList;

/**
 * Created by kristinakosko on 24.03.17.
 */

public class User {

    private String  name;
    private String  surname;
    private String  patronymic;
    private Integer age;
    private String  hometown;
    private String  gender;
    private ArrayList<Integer> preferences;

    public User(String name, String surname, String patronymic, Integer age, String hometown, String gender, ArrayList<Integer> preferences) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.age = age;
        this.hometown = hometown;
        this.gender = gender;
        this.preferences = preferences;
    }

    public User() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<Integer> getPreferences() {
        return preferences;
    }

    public void setPreferences(ArrayList<Integer> preferences) {
        this.preferences = preferences;
    }
}
