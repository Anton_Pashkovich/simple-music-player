package com.simplemobiletools.musicplayer.entity;

/**
 * Created by Moluram on 3/30/2017.
 */

public class Interval implements Comparable<Interval> {

    private Integer left;
    private Integer right;

    public Integer getLeft() {
        return left;
    }

    public Integer getRight() {
        return right;
    }

    public Interval(Integer l, Integer r){
        this.left = l;
        this.right = r;
    }

    @Override
    public int compareTo(Interval i){
        return this.left - i.left;
    }

    public boolean overlap(Interval i) {
        return this.left <= i.right && this.right >= i.left;
    }

    public void setRight(Integer right) {
        this.right = right;
    }

    public void setLeft(Integer left) {
        this.left = left;
    }
}