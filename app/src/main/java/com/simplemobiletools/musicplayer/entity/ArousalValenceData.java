package com.simplemobiletools.musicplayer.entity;

import java.util.Date;

public class ArousalValenceData {

    private Double      x;
    private Double      y;
    private Integer     currentPosition;   // time (ms)

    public ArousalValenceData() {}

    public ArousalValenceData(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public ArousalValenceData(Double x, Double y, int currentPosition) {
        this.x = x;
        this.y = y;
        this.currentPosition = currentPosition;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Integer getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Integer currentPosition) {
        this.currentPosition = currentPosition;
    }
}
