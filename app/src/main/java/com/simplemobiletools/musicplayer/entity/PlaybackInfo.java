package com.simplemobiletools.musicplayer.entity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PlaybackInfo {

    private List<ArousalValenceData>    avList;
    private boolean                     rewind;
    private boolean                     skipped;
    private double                      progress;

    public PlaybackInfo() {
        avList = new ArrayList<>();
    }

    public List<ArousalValenceData> getAvList() {
        return avList;
    }

    public void setAvList(List<ArousalValenceData> avList) {
        this.avList = avList;
    }

    public boolean isRewind() {
        return rewind;
    }

    public void setRewind(boolean rewind) {
        this.rewind = rewind;
    }

    public boolean isSkipped() {
        return skipped;
    }

    public void setSkipped(boolean skipped) {
        this.skipped = skipped;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }
}
