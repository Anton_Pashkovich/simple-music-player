package com.simplemobiletools.musicplayer.entity;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Moluram on 3/27/2017.
 */

public class PlaceInfo {
    private LatLng latLng;
    private List<Integer> typesOfPlace;

    public PlaceInfo setLatLng(LatLng latLng) {
        this.latLng = latLng;
        return this;
    }

    public LatLng getLatLng() {
        return latLng;
    }


    public PlaceInfo setTypesOfPlace(List<Integer> typeOfPlace) {
        this.typesOfPlace = typeOfPlace;
        return this;
    }

    public List<Integer> getTypesOfPlace() {
        return typesOfPlace;
    }
}
